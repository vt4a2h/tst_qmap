#include <QCoreApplication>
#include <QDebug>
#include <QMap>

#include <algorithm>

typedef QMap< QString, qreal > FirstContainer;
typedef QMap< QString, FirstContainer> SecondContainer;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // вставка значений (разные ключи)
    SecondContainer k;
    k[ "m1" ][ "k1" ] = 1.0;
    k[ "m2" ][ "k2" ] = 2.0;
    k[ "m3" ][ "k3" ] = 3.0;

    qDebug() << "#Test 1 -- simple insert\n"
                "Container k:\n"
             << k << "\n";

    // вставка значений (одинаковые ключи)
    // лучше использовать QMultiMap
    auto it = k.insertMulti( "m1", FirstContainer() );
    it->insert( "k1", 4.0 );
    it->insertMulti( "k1", 5.0 ); // если необходимо дублирование
    it->insert( "k2", 1.2 );

    qDebug() << "#Test 2 -- multi insert\n"
                "Container k:\n"
             << k <<
                "\n"
                "Container k[ \"m1\" ]:\n"
             << k[ "m1" ] <<
                "\n"
                "All values k[ \"m1\" ]:\n"
             << k[ "m1" ].values() <<
                "\n"
                "All keys k[ \"m1\" ]:\n"
             << k[ "m1" ].keys() << "\n" ;

    // поиск значений
    // #1 => [m1][k2]
    qreal result = -1;
    if( k.contains( "m1" ) ) {
        if ( k[ "m1" ].contains( "k2" ) ) {
            result = k[ "m1" ][ "k2" ]; // если одинаковых ключей много, то будет первое значение
        }
    }
    qDebug() << "#Test 3 -- first search method\n"
                "Result for k[ \"m1\" ][ \"k2\" ]: "
             << result << "\n";

    // #2 => [m1][k1]
    if ( k.contains( "m1" ) ){
        qDebug() << "#Test 4 -- second search method (all values for k[ \"m1\" ])";
        for( auto it: k[ "m1" ].values() ) {
            qDebug() << it;
        }
        qDebug() << "\n"
                    "#Test 5 -- second search method (all values for k[ \"m1\" ][ \"k1\" ])";
        for( auto it: k[ "m1" ].values( "k1" ) ) {
            qDebug() << it;
        }
    }

    // если необходим детальный поиск, то можно искать, например в k[ "m1" ].values( "k1" )
    auto ex_s = k[ "m1" ].values( "k1" ); // ex_s -- QList< qreal >

    qDebug() << "\n"
                "#Test 6 -- detail search in all values k[ \"m1\" ][ \"k1\" ]\n"
             << "Container contains: " << ex_s ;
    auto index = ex_s.indexOf( 5 );
    if( index != -1 ) qDebug() << "Result for search 5:"
                               << "index -- " << index
                               << ", value -- " << ex_s.at( index ) << "\n";

    // поиск значения по заданному критерию (в данном случае -- поиcк первого значения, которое меньше 5)
    qDebug() << "\n"
                "#Test 7 -- detail search in all values k[ \"m1\" ][ \"k1\" ] (with predicat)\n"
             << "Container contains: " << ex_s ;
    auto it_2 = std::find_if( ex_s.begin(), ex_s.end(),
                            [](qreal a)-> bool {
                                return ( a < 5  ? true : false );
                            }
            );
    if ( it_2 != ex_s.end() ) {
        qDebug() << "First value which less then 5:" << *it_2 << "\n";
    }
    
    return a.exec();
}
