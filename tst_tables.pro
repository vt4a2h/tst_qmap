#-------------------------------------------------
#
# Project created by QtCreator 2013-06-28T15:38:17
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = tst_tables
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

CONFIG += c++11
